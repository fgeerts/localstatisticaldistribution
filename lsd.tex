\documentclass[10pt]{article}

\usepackage{a4wide}
\usepackage{bibentry}
\usepackage{amsmath,amssymb}
\let\Bbbk\relax


\newcommand{\horline}{\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}}

\begin{document}


\nobibliography*

\section*{Related work}
What follows is not an exhaustive literature survey, it just a bunch of papers
found after a quick Google Search. I'll discuss them in chronological order.

\horline
\begin{quote}~\cite{Bayesian}
An ``early" paper is: \textbf{\bibentry{Bayesian}}. Too many details are missing, however, so I cannot
really explain what is going on here. Perhaps someone with a more expert knowledge
on Bayesian networks can understand the approach presented in this paper. It is not technically deep, just messy (and
that explains perhaps why it is unpublished.) 
\end{quote}
\horline

\begin{quote}~\cite{Heller}
In the technical report: \textbf{\bibentry{Heller}}, the emphasis lies on quantitative data, from a statistical point of view,
with an emphasis on outlier detection and exploratory data analysis, and based on robust statistics.
\end{quote}
\horline

\begin{quote}~\cite{Mayfield:2010}
The following paper,
\textbf{\bibentry{Mayfield:2010}}, concerns an iterative statistical framework for data cleaning. Dependencies between
random variables (attribute values of tuples) is modelled by mean of so-called relational dependency networks. In addition,
quantitative dependences are given for each node in the network, and specified as conditional probability distributions.
\end{quote}

\horline
\begin{quote}~\cite{Yakout:2010}
Perhaps not so relevant is the paper: 
\textbf{\bibentry{Yakout:2010}}. I am just including it here because the authors uses some probabilistic computations. The goal is to
provide some guidance during the repairing process. For this purpose, groups of candidate repairs $c=\{\bar r_1,\ldots, \bar r_k\}$
are considered and an update benefit 
$$
b(c)=L(D\mid c)+ \sum_{\bar r_i\in c} \bigl(p_i L(D[\leftarrow \bar r_i]) +(1-p_i) L(D[\nleftarrow \bar r_i])\bigr),
$$
is defined. Here, $p_i$ denotes the probability that a user accepts update $\bar r_i$ to $D$, denoted by $D[\leftarrow \bar r_i]$,
and $D[\nleftarrow \bar r_i]$ denotes when the update is not applied (with probability $1-p_i$). Moreover, $L(\cdot)$ stands
for some data quality loss function (defined in terms of fraction of violations, see details in the paper). One will suggest that group
$c$ of repairs to  the user which maximizes the expected update benefit $\mathbb{E}[b(c)]$. Of course, one does not know the
probabilities $p_i$. Active learning (classifiers) are used to obtain estimates for these probabilities. In this paper,
(conditional) functional dependencies are the chosen data quality constraint language.
\end{quote}

\horline

\begin{quote}~\cite{Berti-Equille:2011}
Next in line is a paper on ``glitches''. A complete data cleaning framework is defined in the paper: 
\textbf{\bibentry{Berti-Equille:2011}}. This is a paper on quantitative cleaning (see also~\cite{Heller,Mayfield:2010}).
Quite a lot of things are going on this paper. 

A glitch in the data can be anything, but examples include missing values,
duplicate entries, inconsistent values. In general, one can extract from each tuple $\bar t$ and attribute $A$ in an instance $I$, a feature
vector $\textsl{glitch}(\bar t,A)$ consist of zero and ones. Here, $\textsl{glitch}(\bar t,A)[i]=1$ iff glitch detection method $i$
identifies a glitch in $\bar t[A]$. One further distinguishes between (a)~multi-type glitches, i.e., for a tuple $\bar t$ and attribute $A$,
 $\textsl{glitch}(\bar t,A)$ contains at least two ``ones''; (b)~concomitant glitches, i.e., for a tuple $\bar t$, at least two attributes
 $A$ and $B$ exists such that  both $\textsl{glitch}(\bar t,A)$ and  $\textsl{glitch}(\bar t,B)$ are non-zero feature vectors; and (c)~multi-occurrent
 glitches, i.e., if  $\textsl{glitch}(\bar t,A)=\textsl{glitch}(\bar s,B)$ for two distinct values $\bar t[A]$ and $\bar s[B]$. It is not clear where
 this distinction between glitch types is used.

One of the aims is to discover \textit{glitch patterns}.  A rather ad-hoc definition of glitch pattern is given in (Sect. IV-C) based
in horizontal and vertical support threshold (in the glitch signature matrix $\mathbf{G}=[ \text{glitch}(\bar t,A)]_{\bar t, A}$) and
some notion of strong independence (measured by $\chi^2$-test) amongst different glitch types.

What may, perhaps, be relevant is how glitch distributions are estimated. It is assumed that glitch types are independent. If
we denote by 
$$
N_i=\sum_{\bar t,A} \text{glitch}(\bar t, A)[i],
$$
the number of glitches of type ``$i$'', and 
$$
N_{i,j}=\sum_{\bar t,A} \text{glitch}(\bar t, A)[i]\cdot \text{glitch}(\bar t, A)[j],
$$
the number of co-occurrence of glitch types ``$i$'' and ``$j$'', then
$$
\chi^2(i,j)=\frac{ \bigl(N_{i,j} - \frac{N_i\cdot N_j}{n\cdot k}\bigr)^2}{\frac{N_i\cdot N_j}{n\cdot k}},
$$
is used to test the disparity between the observed co-occurrences $N_{i,j}$ and what is expected (assuming
independence) $\frac{N_i\cdot N_j}{n\cdot k}$. Here, $n$ denotes the number of tuples, $k$ the number of glitch types.
This can be extended to multiple glitch types. I assume that this leads to the notion of ``strong independence'' used when
defining glitch patterns, but I could not find an explicit definition of this  in the paper. 
An alternative (using Kullback-Leibler) is mentioned in the appendix. Insufficient
details are provided, however, of how this could work.

With regards to repairing, first an aggregate \textit{glitch score} $g({\cal D})=\frac{1}{n\cdot k}\sum_{\bar t, A} g_{\bar t, A}$
is defined, where each 
$$
g_{\bar t,A}=\sum_{i=1}^k w_i\cdot g_{\bar t,A}[i],
$$
for given glitch type weights $w_i$, and where $g_{\bar t,A}[i]$ is defined in terms of some further aggregation of different
methods than can identify glitches of type ``$i$''. A so-called Fleiss-Kappa measure is used here. I don't think this really
matters (it just makes it look complicated and statistically relevant...). Then, for a cleaning strategy $\mathsf{C}$, the
\textit{effectiveness} of $\mathsf{C}$ on ${\cal D}$, is measured as 
$$
\textsl{eff}(\mathsf{C},{\cal D})=\frac{g({\cal D})- g({\cal D}_c)}{g({\cal D}_c)},
$$
where ${\cal D}_c$ is the (clean) database obtained from ${\cal D}$ using $\mathsf{C}$ (also denoted by 
$\mathsf{C}({\cal D})$. This measure is further normalised
as
$$
\textsl{n-eff}(\mathsf{C},{\cal D})=\frac{\textsl{eff}(\mathsf{C},{\cal D})}{\sqrt{\textsl{eff}(\mathsf{C},{\cal D})^2+\alpha}}
$$
with normalisation constant $\alpha\geq 0$. Don't ask me why. Now, what they are finally want to obtain as repair is
the clean database ${\cal D}_c^\star$ satisfying
$$
{\cal D}_c^\star=\arg\min_{\mathsf{C}} \text{dist}(\mathsf{C}({\cal D}), {\cal D}_c^{\textsl{true}})
$$
subject to $\textsl{Cost}(\mathsf{C},{\cal D})\leq \mathbb{B}$ and $\textsl{n-eff}(\mathsf{C},{\cal D})\geq \mathbb{E}$,
for thresholds $\mathbb{B}$ and $\mathbb{E}$. (For definition of cost, see paper Eqn~(11)).  In the optimization
problem, different cleaning strategies are considered and ${\cal D}_c^{\textsl{true}}$ is supposed to  be the ``true''
clean dataset. (Not sure exactly how they get ${\cal D}_c^{\textsl{true}}$...). They use the normalised Kullback-Leibler
distance, between candidate cleaned dataset $\mathsf{C}({\cal D})$ and the true one. As said before, it is not clear
how they define this measure in this context. 

To conclude, it is all a bit messy and ad-hoc. Table II in the paper may be of use. It lists ways of dealing with 
glitches, ranging from doing nothing, deleting them or imputing values using a range of statistical methods.
\end{quote}

\horline

\begin{quote}~\cite{Dasu:2012}
A metric for measuring the effectiveness of data cleaning methods is described in \textbf{\bibentry{Dasu:2012}}. Intuitively,
the effect of repairs of the data on the underlying data distribution is measured. The motivation example (Section 1.1)
shows that when data is assumed to be normally distributed, and outliers are repaired using ``Winsorization'', i.e., the
values of outliers are replaced with their closest ``acceptable'' values, then this clearly distorts the distribution of clean data,
and furthermore, this may shift previously correct values towards the outlier region. As another example, imputing missing
values by the same value (say the mean of the distribution) creates a spike at the mean. An imputation that preserves the
distribution better would be preferred. This paper primarily concerns an experimental framework and their approach is
illustrated (and experimented on) in the setting of data streams. The experimental setup may be worth looking at for
some inspiration. Conceptually, perhaps most relevant is that when a ``cleaning strategy''
$\mathsf{C}$ is applied on dirty data ${\cal D}$, resulting in a cleaned dataset ${\cal D}_c$, the \textit{statistical distortion}
$$
S(\mathsf{C},{\cal D})=\text{dist}({\cal D},{\cal D}_c)
$$
is measured, where $\text{dist}(\cdot)$ is a distance between two empirical distributions. This should be small. In addition, ${\cal D}_C$ should be
cleaner than ${\cal D}$. This is captured by glitch scores and effectiveness, as given in~\cite{Berti-Equille:2011}. 
%
%in which test data is generated
%$\{({\cal D}^i,{\cal D}^i_c) \mid i\in[1,n]\}$, where ${\cal D}_c^i$ is clean data and ${\cal D}_i$ is dirty data. It is either assumed
% that the ${\cal D}_c^i$'s comes from an ideal clean data set ${\cal D}_c$, or that one can somehow extract a clean part from
% the data and treat the remaining part as dirty. One then samples from the clean and dirty part to obtain the test data.
\end{quote}


\horline

\begin{quote}~\cite{DBLP:journals/corr/abs-1204-3677}
In, \textbf{\bibentry{DBLP:journals/corr/abs-1204-3677}}, it is claimed that the number of conditional functional dependencies drops when noise is added.
They propose a fully probabilistic framework for cleaning. Here is how it works. Let $c=\{\bar r_1,\ldots, \bar r_k\}$ candidate repairs for 
tuple $\bar t$ in instance ${\cal D}$. One wants to find
\begin{align*}
\bar r^\star&
=\arg\max_{\bar r_i\in c} \text{\bf Pr}[\bar r_i\mid \bar t]\\
&=\arg\max_{\bar r_i\in c} \text{\bf Pr}[\bar t\mid \bar r_i]\cdot \text{\bf Pr}[\bar r_i].
\end{align*}
Next,  $\text{\bf Pr}[\bar r_i]$ is inferred from a Bayesian network learned over the data ${\cal D}$ (not sure how this works though).
Intuitively, the Bayesian network describes data generation. I assume that you can somehow use this to get a probability of a tuple
(say $\bar r_i$) that is not necessarily in the data? 

For $\text{\bf Pr}[\bar t\mid \bar r_i]$ independence between attributes is assumed and thus
$$
\text{\bf Pr}[\bar t\mid \bar r_i]=\prod_{j=1}^k \text{\bf Pr}[\bar t[A_j]\mid \bar r_i[A_j]].
$$
It remains to learn  $\text{\bf Pr}[\bar t[A_j]\mid \bar r_i[A_j]]$ for each attribute $A_j$. For this, 
$$
\text{\bf Pr}[\bar t[A_j]\mid \bar r_i[A_j]]=\frac{1}{Z} \exp\biggl(\Delta(\bar t[A_j],\bar r_i[A_j])\biggr)
$$
is taken (I am ignoring here parts related to string typo's), where $\Delta(\bar t[A_j],\bar r_i[A_j])$ measured
``distributional similarity''. Its definition is rather contrived. It requires:
\begin{align*}
\text{supp}(S,{\cal D})&=\text{number of tuples that contain all values in $S$}\\
\text{\bf Pr}[S]&=\frac{\text{supp}(S,{\cal D})}{|{\cal D}|}\\
\text{\bf Pr}[S\mid c]&=\frac{\text{supp}(S \cup \{c\},{\cal D})+\mu}{\text{supp}(\{c\},{\cal D})},
\intertext{for some Laplace smoothing factor (???) $\mu$, such that }
\Delta_S(\bar t[A_j],\bar r_i[A_j])&:=\sum_{c\in S}\frac{\text{\bf Pr}(c\mid \{\bar r_i[A_j]\})\cdot \text{\bf Pr}(c\mid \{\bar t[A_j]\})\cdot \text{\bf Pr}(\{\bar t[A_j]\})}{\text{\bf Pr}(c)}.
\end{align*}
Then, $S$ is restricted to values that co-occur in tuples together with $\bar t[A_j]$ and (or??) $\bar r_i[A_j]$.

Now, it is unclear to me how the set $c$ of candidate repairs for $\bar t$ is obtained, but given this set, the repair carried out will be the one 
maximizing the objective function given earlier.
\end{quote}
\horline

\begin{quote}
\bibentry{Yakout:2013}
\end{quote}
\horline


\begin{quote}
\bibentry{6816655}
\end{quote}
\horline


\begin{quote}
\bibentry{phdthesis}
\end{quote}
\horline


\begin{quote}
\bibentry{Prokoshyna:2015}
\end{quote}
\horline


\begin{quote}~\cite{DeHMCK16} The paper,
\textbf{\bibentry{DeHMCK16}}, is an extended version of~\cite{DBLP:journals/corr/abs-1204-3677}, which was discussed previously. One thing
that is made a bit more clear in this extension is how candidate repairs are found. Basically, they extract candidates from the data that are similar
to the dirty tuple under consideration.
\end{quote}
\horline



\begin{quote}
\bibentry{KrishnanWWFG16}
\end{quote}
\horline


\begin{quote}
\bibentry{L}
\end{quote}
\horline



%
\bibliographystyle{plain}
\bibliography{lsd}
%
%
\end{document}